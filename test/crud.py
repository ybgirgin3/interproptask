import unittest

from fastapi.testclient import TestClient

# Import your FastAPI app from main.py
from main import app, start_db_client

start_db_client()

# Create a test client using the TestClient
client = TestClient(app)


class TestPostsRoutes(unittest.TestCase):

    # Test for "GET /posts/"
    def test_get_all_posts(self):
        response = client.get('/posts/all')
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.json(), list)

    # Test for "GET /posts/{id}"
    def test_get_single_post(self):
        # Assuming there is a post with id='some_post_id'
        response = client.get('/posts/find/?id=64baa911fce385084a601197')
        self.assertEqual(response.status_code, 200)
        # self.assertIn('id', response.json())
        self.assertIsInstance(response.json(), dict)

    # Test for "GET /posts/update"
    def test_update_post(self):
        # Assuming there is a post with id='some_post_id'
        post_data = {
            "title": "updated-proglang",
            "short_description": "updated-new-proglang",
            "description": "Let's play a guess game about our new update-lang",
            "tags": [
                "lang",
                "updated",
                "new"
            ],
            "created_at": "2023-07",
            "updated_at": "2023-07"
        }
        response = client.put('/posts/update/?id=64baa911fce385084a601197', json=post_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["title"], post_data["title"])
        self.assertEqual(response.json()["description"], post_data["description"])

    # Test for "POST /posts/create"
    def test_create_post(self):
        # Replace this with the actual post data you want to test
        post_data = {
            "title": "Rust2",
            "short_description": "Rust-lang2",
            "description": "Best lang Rust lang2",
            "tags": [
                "Rust2",
                "lang2"
            ],
            "created_at": "2023-07",
            "updated_at": "2023-07"
        }
        response = client.post('/posts/create', json=post_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()["title"], post_data["title"])
        self.assertEqual(response.json()["description"], post_data["description"])


if __name__ == '__main__':
    unittest.main()
