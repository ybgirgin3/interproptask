from typing import List

from fastapi import APIRouter, Request, status

import src.db.service as db_service
from src.models import posts

# define a router
router = APIRouter(prefix='/posts', tags=['posts'])


@router.get('/all', response_description='get all posts', response_model=List[posts.Post])
async def findall(req: Request):
    """
    __summary__: Find all posts from db
    :param req: Request => GET
    :return: List of posts
    """
    return await db_service.find_all(req, 10)


@router.get('/find', response_description='get a post', response_model=posts.Post)
async def findone(req: Request, id: str):
    """
    ___summary__: Find a Post with given id
    :param req:
    :param id:
    :return: Posts
    """
    return await db_service.find_by_id(req, id)


@router.put('/update/', response_description='update a post', response_model=posts.UpdatePost)
async def update_one(req: Request, id: str, post: posts.UpdatePost):
    """
    __summary__: Update a post
    :param req:
    :param id:
    :param post: UpdatePost
    :return: post
    """
    return await db_service.update_post(req, id, post)


@router.post('/create', response_description='create post', response_model=posts.Post,
             status_code=status.HTTP_201_CREATED)
async def create(req: Request, post: posts.Post):
    """
    __summary__: create a post
    :param req:
    :param post:
    :return:
    """
    return await db_service.create_post(req, post)


@router.delete('/{id}', response_description='delete a Post')
async def delete_one(req: Request, id: str):
    """
    __summary__: Delete a post by id
    :param: req:
    :param: id:
    :return:
    """
    return await db_service.delete_by_id(req, id)
