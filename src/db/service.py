import datetime

from bson import ObjectId
from fastapi import Request, HTTPException, status
from fastapi.encoders import jsonable_encoder

from src.models.posts import Post


def _get_collection(request: Request):
    """
    ! will be a private function
    :param request:
    :return:
    """
    return request.app.database['posts']


def is_exists(request: Request, post: Post):
    return _get_collection(request).find_one(
        {
            'title': post.get('title'),
            'short_description': post.get('short_description'),
            'description': post.get('description'),
            'created_at': post.get('created_at')
        }
    )


async def create_post(request: Request, post: Post):
    """
    __summary_: create post if not exists
    :param request:
    :param post:
    :return: Post | HttpException
    """
    # serialize post object
    post = jsonable_encoder(post)

    if d := is_exists(request, post):
        """ if data already exists return existing data """
        return d

    new_post = _get_collection(request).insert_one(post)
    return _get_collection(request).find_one({"_id": new_post.inserted_id})


async def update_post(request: Request, id: str, post: Post):
    """
    __summary__: update post if exists if not raise 404
    :param request:
    :param id:
    :param post:
    :return: Post | HttpException
    """
    post = {k: v for k, v in post.model_dump().items() if v is not None}
    post['updated_at'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    if len(post) >= 1:
        try:
            _get_collection(request).find_one_and_update({"_id": ObjectId(id)}, {"$set": post})
            if (existing_post := _get_collection(request).find_one({"_id": ObjectId(id)})) is not None:
                return existing_post
        except Exception:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Post Not Found!')


async def find_all(request: Request, limit: int = 10):
    """
    __summary__:
    :param request:
    :param limit:
    :return:  List[Posts]
    """
    return list(_get_collection(request).find(limit=limit))


async def find_by_id(request: Request, id: str):
    """
    __summary__: find a post by id
    :param request:
    :param id:
    :return: Post | HttpException
    """
    if post := _get_collection(request).find_one({"_id": ObjectId(id)}):
        return post
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Post Not Found!')


async def delete_by_id(request: Request, id: str):
    try:
        deleted_post = _get_collection(request).find_one_and_delete({"_id": ObjectId(id)})
        return f'Post with {id} deleted successfully'
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Post not found')
