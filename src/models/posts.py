import datetime
import uuid
from typing import Optional

from pydantic import BaseModel, Field, field_validator


class Post(BaseModel):
    _id: str = Field(default_factory=uuid.uuid4, alias="_id")
    title: str
    short_description: str
    description: str
    tags: list[str]
    created_at: str = datetime.datetime.now()
    updated_at: str = datetime.datetime.now()

    @field_validator(
        'title',
        'short_description',
        'description',
    )
    def cannot_be_empty(cls, v):
        if len(v) == 0:
            raise ValueError('fields can not be empty')
        return v.title()


class UpdatePost(BaseModel):
    title: Optional[str]
    short_description: Optional[str]
    description: Optional[str]
