# InterpropTask

## Crud Backend API with FastAPI
__Description__:
This is a basic crud application using FastAPI with MongoDB, and contains all of the `CREATE`, `READ`, `UPDATE` and `DELETE` methods.

__Techs__:
* [MongoDB Atlas](https://www.mongodb.com/atlas/database)
* [FastAPI](https://fastapi.tiangolo.com/)
* [Uvicorn](https://www.uvicorn.org/)

## How Can I Make This Run?

* setup a virtual environment
```shell
python3 -m venv venv && source venv/bin/activate
```

* install required packages
```shell
pip3 install -r requirements.txt
```

* and simply run main.py. This method uses Uvicorn to handle server

```shell
python3 main.py
```

## How Can I Test?

After you setup your virtual environment; run following command

```shell
python3 -m unittest discover -s test -p 'crud.py'
```

and if you did everything right, you'll see the server could pass all the 3 test :)
